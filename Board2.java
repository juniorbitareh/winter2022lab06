public class Board2 {
    private Die die1;
    private Die die2;
    private boolean[] closedTiles;

    public Board2(){
    this.die1= new Die();
    this.die2= new Die();
    this.closedTiles= new boolean[12];
    for (int i=0; i<12; i++){
        closedTiles[i]=false;
        }
    }

    public String toString(){
        String ct="";
        for (int i=0; i<12; i++){
            if (!(closedTiles[i])){
                ct = ct + " " + (i+1);
            }
            else{
                ct = ct + " X";
            }
        }
        return (ct);
        }
        
    public boolean playATurn() {
    die1.roll();
    die2.roll();
    System.out.println(die1.toString());
    System.out.println(die2.toString());
    int sumPips= die1.getPips() + die2.getPips();
    if (!(this.closedTiles[sumPips-1])) {
        this.closedTiles[sumPips-1]=true;
        System.out.println("Closing Tile: " + sumPips);
        return(false);
    }
    else {
        System.out.println("The tile: " + sumPips + " is already shut");
        return(true);
        }
    }

}