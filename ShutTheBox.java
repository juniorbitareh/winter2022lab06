public class ShutTheBox {
    public static void main(String[]args){
        System.out.println("Welcome to the Shut The Box Game!");
        Board2 bo = new Board2();
        boolean GameOver = false;
        while  (!(GameOver))  {
            System.out.println("Player 1's turn");
            System.out.println(bo.toString());
            boolean bool = bo.playATurn();
            if (bool){
                System.out.println("Player 2 Wins");
                GameOver=true;
            } 
            else{
                System.out.println(bo.toString());
                boolean bool2 = bo.playATurn();
                if (bool2){
                    System.out.println("Player 1 Wins");
                    GameOver=true;
                } 

            }
            
        }
    }
}
